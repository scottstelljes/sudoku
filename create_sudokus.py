import numpy as np
from sudoku.generate import generate_puzzle, clue_count

number_of_iterations = 100000
max_number_clues = 29
number_puzzles = 0

# mode w for creating a new file, a for appending
mode = "a"

outfile = open("data/sudoku_puzzles.txt", mode)

for j in range(number_of_iterations):
    if j % 100 == 0:
        print("{0} iterations of {1} complete. {2} puzzles have been added.".format(j, number_of_iterations,
                                                                                    number_puzzles))
    puzzle = generate_puzzle()
    if clue_count(puzzle) <= max_number_clues:
        outfile.writelines(puzzle + '\n')
        number_puzzles = number_puzzles + 1

outfile.close()
