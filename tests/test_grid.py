from sudoku.grid import *
import numpy as np


def test_row():
    board = tuple(range(81))
    assert row(board, 0) == (0, 1, 2, 3, 4, 5, 6, 7, 8)
    assert row(board, 1) == (9, 10, 11, 12, 13, 14, 15, 16, 17)
    assert row(board, 2) == (18, 19, 20, 21, 22, 23, 24, 25, 26)
    assert row(board, 3) == (27, 28, 29, 30, 31, 32, 33, 34, 35)
    assert row(board, 4) == (36, 37, 38, 39, 40, 41, 42, 43, 44)
    assert row(board, 5) == (45, 46, 47, 48, 49, 50, 51, 52, 53)
    assert row(board, 6) == (54, 55, 56, 57, 58, 59, 60, 61, 62)
    assert row(board, 7) == (63, 64, 65, 66, 67, 68, 69, 70, 71)
    assert row(board, 8) == (72, 73, 74, 75, 76, 77, 78, 79, 80)


def test_column():
    board = tuple(range(81))
    assert column(board, 0) == (0, 9, 18, 27, 36, 45, 54, 63, 72)
    assert column(board, 1) == (1, 10, 19, 28, 37, 46, 55, 64, 73)
    assert column(board, 2) == (2, 11, 20, 29, 38, 47, 56, 65, 74)
    assert column(board, 3) == (3, 12, 21, 30, 39, 48, 57, 66, 75)
    assert column(board, 4) == (4, 13, 22, 31, 40, 49, 58, 67, 76)
    assert column(board, 5) == (5, 14, 23, 32, 41, 50, 59, 68, 77)
    assert column(board, 6) == (6, 15, 24, 33, 42, 51, 60, 69, 78)
    assert column(board, 7) == (7, 16, 25, 34, 43, 52, 61, 70, 79)
    assert column(board, 8) == (8, 17, 26, 35, 44, 53, 62, 71, 80)


def test_box():
    board = tuple(range(81))
    assert box(board, 0) == (0, 1, 2, 9, 10, 11, 18, 19, 20)
    assert box(board, 1) == (3, 4, 5, 12, 13, 14, 21, 22, 23)
    assert box(board, 2) == (6, 7, 8, 15, 16, 17, 24, 25, 26)
    assert box(board, 3) == (27, 28, 29, 36, 37, 38, 45, 46, 47)
    assert box(board, 4) == (30, 31, 32, 39, 40, 41, 48, 49, 50)
    assert box(board, 5) == (33, 34, 35, 42, 43, 44, 51, 52, 53)
    assert box(board, 6) == (54, 55, 56, 63, 64, 65, 72, 73, 74)
    assert box(board, 7) == (57, 58, 59, 66, 67, 68, 75, 76, 77)
    assert box(board, 8) == (60, 61, 62, 69, 70, 71, 78, 79, 80)
