from sudoku.generate import *
from sudoku.grid import row, column, box

def test_generate_puzzle():
    assert is_solvable(generate_puzzle()[:81])


def test_generate_sudoku():
    grid = generate_sudoku()
    labels = list(range(1, 10))
    assert len(grid) == 81
    assert all(grid[k] in labels for k in range(81))
    assert all(one_of_each(row(grid, k), labels) for k in range(9))
    assert all(one_of_each(column(grid, k), labels) for k in range(9))
    assert all(one_of_each(box(grid, k), labels) for k in range(9))


def test_array_to_string():
    my_arr = np.array([1, 2, 3])
    assert array_to_string(my_arr) == '123'


def test_generate_random_clue_pattern():
    cpat = generate_random_clue_pattern()
    assert cpat.shape == (81,)
    assert all(k in {0, 1} for k in cpat)


def test_is_solvable():
    s1 = "030467050920010006067300148301006027400850600090200400005624001203000504040030702"
    s2 = "030000050000010006067300100301006000000850600090200400005624001203000504040030702"
    assert is_solvable(s1)
    assert not is_solvable(s2)


def test_clue_count():
    p = "030467050920010006067300148301006027400850600090200400005624001203000504040030702"
    assert clue_count(p) == 40


def one_of_each(x, labels):
    return set(x) == set(labels)
