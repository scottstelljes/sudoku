# sudoku

Create uniquely solvable Sudoku puzzles with solutions.

The results of this project can be used to create a Sudoku app. One use of this app would be to get users to provide difficulty ratings. The user ratings could then be used as targets in a machine learning model to predict the difficulty of puzzles.