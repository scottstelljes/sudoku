import os
import datetime

now = datetime.datetime.now()
CURRENT_YEAR = now.year
CURRENT_MONTH = now.month
CURRENT_DAY = now.day

# Path info
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_DIR = os.path.join(ROOT_DIR, 'tests')

# Random seed - use an integer to make the calculations reproducible, None to randomize it.
RANDOM_SEED = None
