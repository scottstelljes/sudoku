import numpy as np
import pandas as pd
from sudoku.generate import clue_count


puzzles = np.loadtxt("data/sudoku_puzzles.txt", dtype=str)

number_of_clues = [clue_count(p) for p in puzzles]

puzzle_df = pd.DataFrame({"puzzle": puzzles, "number_of_clues": number_of_clues})

puzzle_df['group'] = (puzzle_df['number_of_clues'] - 26) // 2

df1 = puzzle_df[puzzle_df['number_of_clues'] > 25].copy()

df2 = df1.groupby('group').apply(lambda x: x.sample(500))

print(df2['number_of_clues'].value_counts())