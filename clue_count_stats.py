import numpy as np
import pandas as pd
from sudoku.generate import clue_count

puzzles = np.loadtxt("data/sudoku_puzzles.txt", dtype=str)

number_of_clues = [clue_count(p) for p in puzzles]

puzzle_df = pd.DataFrame({"puzzle": puzzles, "number_of_clues": number_of_clues})

print(puzzle_df['number_of_clues'].value_counts().sort_index())

print(puzzle_df['number_of_clues'].describe())
