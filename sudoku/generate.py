from random import sample
from dlxsudoku import Sudoku
import numpy as np


def generate_puzzle():
    sudoku_grid = generate_sudoku()
    pattern = np.zeros(81)
    while (not is_solvable(array_to_string(sudoku_grid * pattern))):
        pattern = np.where((pattern + generate_random_clue_pattern()) > 0, 1, 0)
    return array_to_string(sudoku_grid * pattern) + array_to_string(sudoku_grid)


def generate_sudoku():
    rows = [g * 3 + r for g in shuffle(range(3)) for r in shuffle(range(3))]
    cols = [g * 3 + c for g in shuffle(range(3)) for c in shuffle(range(3))]
    labels = shuffle(range(1, 10))
    return np.array([labels[pattern(r, c)] for c in cols for r in rows])


def shuffle(s):
    return sample(s, len(s))


def pattern(r, c):
    return (3 * (r % 3) + r // 3 + c) % 9


def array_to_string(arr):
    return ''.join([str(d) for d in list(arr)])


def generate_random_clue_pattern():
    mat = np.zeros(81)
    pos1 = np.random.randint(81)
    mat[pos1] = 1
    pos2 = (80 - pos1) % 81
    mat[pos2] = 1
    return np.where(mat > 0, 1, 0)


def is_solvable(s):
    try:
        sudoku = Sudoku(s)
        sudoku.solve()
    except:
        return False
    return True


def clue_count(p):
    return sum(int(d) > 0 for d in p[:81])
