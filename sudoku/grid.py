""" Grid functions """
import numpy as np


def row(sudoku, r):
    return tuple(sudoku[r * 9 + k] for k in range(9))


def column(sudoku, c):
    return tuple(sudoku[k * 9 + c] for k in range(9))


def box(sudoku, g):
    r = g // 3
    c = g % 3
    return tuple(sudoku[row * 9 + col + r * 27 + c * 3] for row in range(3) for col in range(3))
